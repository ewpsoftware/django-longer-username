from django.conf import settings
from django.contrib.auth.forms import AuthenticationForm

def MAX_USERNAME_LENGTH():
    if hasattr(settings,"MAX_USERNAME_LENGTH"):
        return settings.MAX_USERNAME_LENGTH
    else:
        return 255
        
# Added so the login field can take email addresses longer than 30 characters
AuthenticationForm.base_fields['username'].max_length = MAX_USERNAME_LENGTH()
AuthenticationForm.base_fields['username'].widget.attrs['maxlength'] = MAX_USERNAME_LENGTH()
AuthenticationForm.base_fields['username'].validators[0].limit_value = MAX_USERNAME_LENGTH()